$(document).ready(function() {

	$(".js-fancybox a[rel]").fancybox({
		helpers : {
			overlay: {
				locked: false
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		},
		nextEffect: 'fade',
		prevEffect: 'fade',
		openEffect: 'fade',
		closeEffect: 'fade'
	});

	$('.js-scrollbar').perfectScrollbar({
		wheelPropagation: false,
		swipePropagation: false
	});

	var Toggle  = {
		init: function() {
			this.btn = ".js-toggle";
			this.$btn = $(this.btn);
			this.$root = $("body");
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$btn.on("click", this._clickAction.bind(this));
		},
		_clickAction: function(event) {
			var $active = $(event.currentTarget);
			var targetClass = $active.data("toggle");
			var rootClass = $active.data("body-class");

			$(targetClass).toggleClass("is-active");
			$(this.btn+'[data-toggle="'+targetClass+'"]').toggleClass("is-active");

			if (rootClass) {
				this.$root.toggleClass(rootClass);
			}


			return false;
		}
	}

	Toggle.init();

	var Nav  = {
		init: function() {
			this.btn = ".js-nav a";
			this.$btn = $(this.btn);
			this.$win = $(window);
			this.$root = $('html, body');
			this.$target = $('[data-id]');
			this.headerHeight = $('.header').outerHeight();
			this._afterLoad();
			this._bindEvents();
		},
		_bindEvents: function() {
			this.$btn.on("click", this._scrollTo.bind(this));
			this.$win.on("scroll", this._changeNav.bind(this));
			$('body').on('section-reached', this._changeNav.bind(this));
		},
		_scrollTo: function(event) {
			var $active = $(event.currentTarget);
			var $target = $('[data-id="'+$active.attr("href")+'"]');
			var top = $target.offset().top - this.headerHeight;
			this.$btn.removeClass("is-active");
			$active.addClass("is-active");
			this.$root.animate({
	            scrollTop: top
	        }, 500);
		},
		_changeNav: function(event) {
			var _this = this;
			var id = $('body').sectionScroll.activeSection[0].dataset.id;
			this.$btn.removeClass("is-active");
			$('[href="'+id+'"]').addClass("is-active");
		},
		_afterLoad: function() {
			var hash = window.location.hash;
			if (hash) {
				var $target = $('[data-id="'+hash+'"]');
				var top = $target.offset().top - this.headerHeight;
				this.$root.animate({
		            scrollTop: top
		        }, 500);
			}
		},
		_changeHash: function(id) {
			window.location.hash = id;
		}
	}

	Nav.init();
	

});